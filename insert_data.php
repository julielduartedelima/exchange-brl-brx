<form id="insert-form">
  <label for="email">Email:</label>
  <input type="email" id="email" name="email" required>

  <label for="password">Senha:</label>
  <input type="password" id="password" name="password" required>

  <label for="saldo_brl">Saldo BRL:</label>
  <input type="number" id="saldo_brl" name="saldo_brl" required>

  <label for="saldo_brx">Saldo BRX:</label>
  <input type="number" id="saldo_brx" name="saldo_brx" required>

  <input type="submit" value="Enviar">
</form>

<script>
document.getElementById('insert-form').addEventListener('submit', function (event) {
  event.preventDefault();

  const email = document.getElementById('email').value;
  const password = document.getElementById('password').value;
  const saldo_brl = document.getElementById('saldo_brl').value;
  const saldo_brx = document.getElementById('saldo_brx').value;

  const xhr = new XMLHttpRequest();
  xhr.open('POST', 'insert.php', true);
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
      alert(xhr.responseText);
    }
  };
  xhr.send(`email=${email}&password=${password}&saldo_brl=${saldo_brl}&saldo_brx=${saldo_brx}`);
});
</script>
