<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Bolsa de Valores</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Bolsa de Valores</h1>
	
	<form action="autenticar_usuario.php" method="post">
  <div>
    <label for="email">E-mail:</label>
    <input type="email" id="email" name="email" required>
  </div>
  <div>
    <label for="senha">Senha:</label>
    <input type="password" id="senha" name="senha" required>
  </div>
  <button type="submit">Entrar</button>
</form>

    <div id="negociacao-container">
      <select id="ativo-a">
        <option value="ativo-a">Ativo A</option>
        <option value="ativo-b">Ativo B</option>
      </select>
      <input type="text" id="quantidade-a" placeholder="Quantidade Ativo A">
      <br><br>
      <select id="ativo-b">
        <option value="ativo-a">Ativo A</option>
        <option value="ativo-b">Ativo B</option>
      </select>
      <input type="text" id="quantidade-b" placeholder="Quantidade Ativo B">
      <br><br>
      <button id="trocar">Trocar</button>
    </div>
    <script src="script.js"></script>
  </body>
</html>
