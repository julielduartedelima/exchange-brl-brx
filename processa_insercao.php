<html>
  <head>
    <title>Inserção de valores</title>
  </head>
  <body>
    <form action="processa_insercao.php" method="post">
      <label for="email">E-mail:</label>
      <input type="email" name="email" id="email" required><br><br>

      <label for="brl">Saldo BRL:</label>
      <input type="number" name="brl" id="brl" step="0.01" required><br><br>

      <label for="brx">Saldo BRX:</label>
      <input type="number" name="brx" id="brx" step="0.01" required><br><br>

      <input type="submit" value="Inserir">
    </form>
  </body>
</html>
