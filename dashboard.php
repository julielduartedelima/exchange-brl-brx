<?php

session_start();

if (!isset($_SESSION["email"])) {
  header("Location: index.php");
  exit;
}

$servername = "localhost";
$username = "username";
$password = "password";
$dbname = "database_name";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$email = $_SESSION["email"];

$sql = "SELECT saldo_brl, saldo_brx FROM users WHERE email = '$email'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  $row = $result->fetch_assoc();
  $saldo_brl = $row["saldo_brl"];
  $saldo_brx = $row["saldo_brx"];
} else {
  echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();

?>

<h1>Seu Saldo</h1>

<p>Saldo BRL: <?php echo $saldo_brl; ?></p>
<p>Saldo BRX: <?php echo $saldo_brx; ?></p>

<a href="index.php">Voltar para o início</a>
