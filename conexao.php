<?php
// Conexão com o banco de dados
$conn = mysqli_connect('localhost', 'usuario_banco', 'senha_banco', 'bolsa_valores');

// Verifica se as informações foram enviadas
if (isset($_POST['email']) && isset($_POST['senha'])) {
  $email = mysqli_real_escape_string($conn, $_POST['email']);
  $senha = mysqli_real_escape_string($conn, $_POST['senha']);

  // Verifica se o usuário existe
  $query = "SELECT id, saldo_brl, saldo_brx FROM usuarios WHERE email = '$email' AND senha = '$senha'";
  $result = mysqli_query($conn, $query);
  if (mysqli_num_rows($result) > 0) {
    $usuario = mysqli_fetch_assoc($result);

    // Inicia a sessão e armazena informações do usuário
    session_start();
    $_SESSION['usuario_id'] = $usuario['id'];
    $_SESSION['saldo_brl'] = $usuario['saldo_brl'];
    $_SESSION['saldo_brx'] = $usuario['saldo_brx'];

    // Redireciona para a página principal
    header('Location: index.php');
    exit;
  } else {
    echo 'E-mail ou senha inválidos';
  }
}
?>
