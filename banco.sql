CREATE DATABASE bolsa_valores;
USE bolsa_valores;

CREATE TABLE usuarios (
  id INT AUTO_INCREMENT PRIMARY KEY,
  email VARCHAR(255) NOT NULL UNIQUE,
  saldo_brl DECIMAL(10,2) NOT NULL DEFAULT 0,
  saldo_brx DECIMAL(10,2) NOT NULL DEFAULT 0
);

CREATE TABLE transacoes (
  id INT AUTO_INCREMENT PRIMARY KEY,
  id_usuario INT NOT NULL,
  ativo VARCHAR(10) NOT NULL,
  quantidade DECIMAL(10,2) NOT NULL,
  tipo ENUM('compra', 'venda') NOT NULL,
  data_hora DATETIME NOT NULL,
  FOREIGN KEY (id_usuario) REFERENCES usuarios(id)
);
